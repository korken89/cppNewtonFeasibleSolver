# A C++ library for a Feasible Newton Solver

## Contributors

* Emil Fresk

---

## License

Licensed under the LGPL-v3 license, see LICENSE file for details.
